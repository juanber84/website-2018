'use strict'

const express = require('express')
const path = require('path')
const app = express()
const port = 5000
const indexPath = './_dist/index.html'
const assetsPath = './_dist/assets'

app.use('/assets', express.static(path.join(__dirname, assetsPath)))
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, indexPath))
})

app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`)
})