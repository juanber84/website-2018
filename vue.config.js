// vue.config.js
module.exports = {
    pages: {
        index: {
            entry: 'src/app/main.js',
            template: 'src/app/public/index.html',
            filename: 'index.html'
        },
    }
}